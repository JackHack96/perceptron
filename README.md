# Perceptron

An implementation of the perceptron-based full adder as shown 
at [http://neuralnetworksanddeeplearning.com/chap1.html](http://neuralnetworksanddeeplearning.com/chap1.html) 
for Xilinx PYNQ Z1.

The `Perceptron_AXI_slave` directory contains other three directories:
- `Perceptron_PYNQ`\
  It contains the final project, ready to be synthesized and deployed on the PYNQ.
  It consist of a Zynq-based block design, with a series of perceptron linked together to form a full adder, linked through AXI bus.
- `Perceptron_VHDL`\
  It contains the VHDL implementation + simulation of the perceptron-based full adder block
- `Perceptron_VHDL_IP`\
  It's just the perceptron IP package that's included in `Perceptron_PYNQ`
