library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.all;

entity NeuralNetwork is
port(
        a: in STD_LOGIC;
        b: in STD_LOGIC;
        c: out STD_LOGIC;
        carry: out STD_LOGIC
);
end NeuralNetwork;

architecture Behavioral of NeuralNetwork is

	component Perceptron is
        port (
            a : in STD_LOGIC;
            b : in STD_LOGIC;
            c : out STD_LOGIC
        );
    end component;
    
    signal temp1 : STD_LOGIC;
    signal temp2 : STD_LOGIC;
    signal temp3 : STD_LOGIC;
    
    signal out1 : STD_LOGIC;
    signal out2 : STD_LOGIC;
    signal out_useless : STD_LOGIC;
    signal input4 : STD_LOGIC;
 
begin
    PC1: Perceptron port map(
        a => a,
        b => b,
        
        c => temp1
    );
    
    PC2: Perceptron port map(
        a => a,
        b => temp1,
        
        c => temp2
    );
    
    PC3: Perceptron port map(        
        a => temp1,
        b => b,
        
        c => temp3
    );
    
    PC4: Perceptron port map(
        a => temp1,
        b => temp1,
        
        c => carry
    );
    
    PC5: Perceptron port map(
        a => temp2,
        b => temp3,
        
        c => c
    );

end Behavioral;
