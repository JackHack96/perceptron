library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.NUMERIC_STD.all;

entity Perceptron is
	port (    
        a : in STD_LOGIC;
        b : in STD_LOGIC;
        c : out std_logic
	);
end Perceptron;

architecture BEHAVIORAL of Perceptron is
constant minustwo : std_logic_vector(2 downto 0) := "110";
constant three : std_logic_vector(2 downto 0) := "011";

begin

process(a,b)
variable  product : std_logic_vector(5 downto 0);
variable  a_logic_vector: std_logic_vector(2 downto 0);
variable  b_logic_vector: std_logic_vector(2 downto 0);
begin

    a_logic_vector:= "00"&a;
    b_logic_vector:= "00"&b;
    --product:= std_logic_vector( ((signed(a_logic_vector)* signed(minustwo)) +  (signed(b_logic_vector)* signed(minustwo))) + 3);
    product := std_logic_vector( (signed(a_logic_vector) * signed(minustwo)) + (signed(b_logic_vector) * signed(minustwo)) + signed(three));
    c<= not product(5);

end process;
--product := STD_LOGIC_VECTOR((((SIGNED(a) * minustwo) + (SIGNED(b) * minustwo)) + 3));


    
    
    
end architecture;