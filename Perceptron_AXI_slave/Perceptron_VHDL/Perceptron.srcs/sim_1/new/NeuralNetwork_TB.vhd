library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity NeuralNetwork_TB is
	-- Port ();
end NeuralNetwork_TB;

architecture NN of NeuralNetwork_TB is

    signal clk : STD_LOGIC;
    
    signal a : STD_LOGIC;
    signal b : STD_LOGIC;
    signal c : STD_LOGIC;
    signal carry : STD_LOGIC;
	
	signal counter       : integer := 0;

	component NeuralNetwork is
        port (
                a: in STD_LOGIC;
                b: in STD_LOGIC;
                c: out STD_LOGIC;
                carry: out STD_LOGIC
        );
    end component;

begin
    pc: NeuralNetwork port map(
        a => a,
        b => b,
        c => c,
        carry => carry
    );

    process -- clock process
    begin
        clk <= '0';
        wait for 25 ns;
        clk <= '1';
        wait for 25 ns;
    end process;

    process(clk)
    begin
        if clk'EVENT and clk = '1' then
            counter <= counter + 1;
            case counter is
                when 0 =>
                    a <= '0';
                    b <= '0';
                when 1 =>
                    a <= '1';
                    b <= '0';
                when 2 =>
                    a <= '0';
                    b <= '1';
                when 3 =>
                    a <= '1';
                    b <= '1';
                when others =>
                    a <= '0';
                    b <= '0';
            end case;
        end if;
    end process;

end NN;